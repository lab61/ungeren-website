# TO-DO
* Build Sanity schema element on editing and adding groups in the house
* Build Sanity schema under "Pages" about being an activist
* Write content for "Bliv aktivist" about monday meeting and overview of groups
* Build new page "Group"
* Write content for "Om Ungdomshuset" incl. "Ungerens historie"
* Build new page "Kontakt" with general, booking, kichen etc.
* Write content for "Støt Ungdomshuset"
* 

# The Ungeren Website

This is the documentation for the Ungeren website. It provides an overview of how the website is developed, build and deployed, without going into details about the individual frameworks used.

## Setup
The website is a headless CMS system based on Next.js with TailwindCSS, deployed by Vercel (including Turborepo), managed on Sanity and stored on Gitlab. 

`Sanity`: Is a headless CMS hosted in the cloud. It is used for adding and updating the website content. Eg. adding a new show to the calendar, changing the weekly activities etc.

`Next.js`: Is a "meta framework" built on top of the React framework. It comes with routing and it able to generate static pages at build time.

`Turborepo`: Turborepo is a build and bundler system. It is used during local development and when building out the website for production. See the `tubrorepo.md` file for details.

`TailwindCSS`: Tailwind is a utility-first CSS framework. Contrary to other CSS frameworks, like Bootstrap, it doesn't come with predefined components.

`Framer Motion`: Framer Motion is an animatiopn library. It is primarily used for page transitions on the Ungeren website.

`GitHub`: The website source code is hosted with GitHub.

`Vercel`: The website is hosted with Vercel. Do note that the Sanity Studio is hosted separately.

## Deployment
This is a headless CMS, so there is no traditional webserver hosting the webpage to access from a client browser. Instead the webpage is automatically generated on the client in order to not be in need of any servers. The code and static files are physically hosted between Gitlab, Vercel and Sanity.
Vercel executes the deployment, when triggered by an update on either Gitlab or Sanity. So when the code is updated on Gitlab, Vercel pulls the new code and generates a static version of the webpage.When new content is added to Sanity, Vercel pulls the data and generates a static version of the webpage.

## Installation guide

## Content editing
These are the instructions on how you should update the webpage.

### New content (to existing schemas)
When you only want to add content to an existing schema, you can simply update the page through the user interface in Sanity Studio. So you do not need to be able to code, but you can only edit content on existing elements as the Calendar or the News page.

#### Prerequisites
None.

#### Steps
1. Go to Sanity Studio (ungeren.sanity.studio)
2. Choose the element you want to edit (Calendar, Page, etc.)
3. Fill out the form
4. Click Publish

### New schemas
When you need a new element on the webpage it needs to be built into the code as a new Typescript schema as well as building it into the SanityClient. 

#### Prerequisites
* You must have cloned the git repository to edit the code
* You must have pnpn installed to serve a local host of Sanity and the webpage

#### Steps
1. Create a new field in ungeren-website/apps/studio/schemas with a saying title 
2. Pull-Add-Commit-Push to Gitlab
3. Fill out the new field in Sanity Studio
4. Create your new schema in ungeren-website/apps/web/pages 
5. Pull-Add-Commit-Push to Gitlab
6.  

## Editing platforms
* Link to 
* 

## Documentation
* 
 
