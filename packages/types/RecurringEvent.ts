export type RecurringEvent = {
  _id: string;
  calendarTitle: string;
  title: string;
  weekday: number;
  startTime: number;
  endTime: number;
  place: string;
  image: any;
  slug: {
    _type: string;
    current: string;
  };
  content: any;
};
