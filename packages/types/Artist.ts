export type Artist = {
  _key: string;
  name: string;
  origin: string;
  link: string;
  bandcamp_track_id: string;
};
