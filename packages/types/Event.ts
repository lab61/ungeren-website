import { Artist } from './Artist';

export type Event = {
  _id: string;
  title: string;
  date: string;
  place: string;
  price: string;
  slug: {
    current: string;
  };
  event_type: {
    _ref: string;
    _type: string;
  } | null;
  image: any;
  artists: Artist[];
  content: any;
};
