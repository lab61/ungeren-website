import sanityClient from '@sanity/client';

const client = sanityClient({
  projectId: 'lfobj7pl',
  dataset: 'production',
  apiVersion: '2021-10-21',
  useCdn: false
});

export default client;
