import format from 'date-fns/format';
import { da } from 'date-fns/locale';
import parseISO from 'date-fns/parseISO';

const UTC_OFFSET = 1; // in hours

export const decodeHtmlEntities = (input: string) =>
  input.replace(/&amp;/g, '&').replace(/&quot;/g, '"');

export const formatEventDate = (inputDate: string) => {
  const date = parseISO(inputDate);
  const baseTzOffset = UTC_OFFSET * 60;
  const tzOffset = date.getTimezoneOffset();
  const newDate = new Date(
    date.valueOf() + (baseTzOffset + tzOffset) * 60 * 1000
  );
  return format(newDate, "EEEE d. MMM yyyy 'kl.' HH:mm", {
    locale: da
  });
};

export const formatNewsDate = (inputDate: string) => {
  const date = parseISO(inputDate);
  const baseTzOffset = UTC_OFFSET * 60;
  const tzOffset = date.getTimezoneOffset();
  const newDate = new Date(
    date.valueOf() + (baseTzOffset + tzOffset) * 60 * 1000
  );
  return format(newDate, 'EEEE d. MMM yyyy', {
    locale: da
  });
};
