import imageUrlBuilder from '@sanity/image-url';
import sanityClient from '../sanityClient';

const builder = imageUrlBuilder(sanityClient);

export const getImage = (source: any) => builder.image(source);
export const getImageUrl = (source: any) => builder.image(source).url();
