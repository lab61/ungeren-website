import { LayoutGroup } from 'framer-motion';
import React, { ReactElement } from 'react';
import { Event, EventType, Page, RecurringEvent } from 'types';
import Banner, { BannerProps } from '../components/Banner/Banner';
import { Calendar } from '../components/Calendar/Calendar';
import Footer from '../components/Footer/Footer';
import HouseRules, {
  HouseRulesProps
} from '../components/HouseRules/HouseRules';
import Layout from '../components/Layout/Layout';
import { NavigationProps } from '../components/Navigation/Navigation';
import { Seo } from '../components/Seo/Seo';
import { decodeHtmlEntities, formatEventDate } from '../helpers/eventHelper';
import { getImageUrl } from '../helpers/imageHelper';
import sanityClient from '../sanityClient';
import { NextPageWithLayout } from './_app';

type Props = NextPageWithLayout & {
  page: Page;
  events: Event[];
  eventTypes: EventType[];
  recurringEvents: RecurringEvent[];
};

const Homepage = (props: Props) => {
  return (
    <React.Fragment>
      <Seo pageTitle={props.page.title} pageUrl="/" seo={props.page.seo} />

      <Calendar
        events={props.events}
        eventTypes={props.eventTypes}
        recurringEvents={props.recurringEvents}
      />
    </React.Fragment>
  );
};

Homepage.getLayout = function getLayout(page: ReactElement) {
  return (
    <React.Fragment>
      {page.props.page.image && (
        <div
          className="relative flex items-center justify-center h-36 md:h-52 lg:h-60 bg-no-repeat bg-cover md:mb-10 xl:mb-20"
          style={{
            backgroundImage: `url(${getImageUrl(page.props.page.image)})`
          }}
        >
          <img
            src="/assets/images/ungdomshuset.png"
            alt="Ungdomshuset text logo"
            className="lg:h-[100px] max-w-[300px] md:max-w-md lg:max-w-full"
          />
        </div>
      )}

      <LayoutGroup>
        <Layout navigation={page.props.navigation} showFooter={false}>
          {page}

          <Banner {...page.props.banner} />
        </Layout>

        <HouseRules {...page.props.houseRules} />
        <Footer />
      </LayoutGroup>
    </React.Fragment>
  );
};

export default Homepage;

export async function getStaticProps() {
  const query = `
  {
    'page': *[_type == "page" && slug.current == "/"]
    {
      _id,
      title,
      slug,
      image,
      content,
      seo
    }[0],

    'events': *[_type == "event" && date >= now()]
    {
      _id,
      title,
      date,
      price,
      place,
      event_type,
      slug,
      image,
      artists,
      content
    } | order(date asc),

    'eventTypes': *[_type == "event_type"]
    {
      _id,
      sortOrder,
      name
    } | order(sortOrder asc),

    'recurringEvents': *[_type == "recurring_event"]
    {
      _id,
      calendarTitle,
      weekday,
      startTime,
      endTime,
      place,
      slug
    } | order(weekday asc, startTime asc),

    'banner': *[_type == "siteSettings"][0]
    {
      homepageBannerRef -> {
        _id,
        title,
        image,
        content,
        linkRef->{
          slug {
            current
          }
        },
        linkText
      }
    },

    'navigation': *[_type == "siteSettings"][0]
    {
      navigationLinks[]{
        _type,
        _key,
        linkRef->{
          slug {
            current
          }
        },
        linkText,
        linkIcon
      },
      facebookUrl,
      instagramUrl
    },

    'houseRules': *[_type == "siteSettings"][0]
    {
      rulesTitle,
      rulesText,
      rulesList,
      rulesLinkRef->,
      rulesLinkText
    }
  }
  `;

  const result = await sanityClient.fetch<{
    page: Page;
    events: Event[];
    eventTypes: EventType[];
    recurringEvents: RecurringEvent[];
    banner: {
      homepageBannerRef: BannerProps;
    };
    navigation: NavigationProps;
    houseRules: HouseRulesProps;
  }>(query);

  const events = result.events.map((event) => ({
    ...event,
    title: decodeHtmlEntities(event.title),
    // format at build time to avoid hydration mismatch
    date: formatEventDate(event.date)
  }));

  return {
    props: {
      page: result.page,
      events,
      eventTypes: result.eventTypes,
      recurringEvents: result.recurringEvents,
      navigation: result.navigation,
      houseRules: result.houseRules,
      banner: result.banner.homepageBannerRef
    },
    revalidate: 60 * 10 // 10 minutes
  };
}
