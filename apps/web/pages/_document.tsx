import { Head, Html, Main, NextScript } from 'next/document';

export default function Document() {
  return (
    <Html lang="da">
      <Head />
      <body className="pt-[80px] md:pt-0">
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
