import { GetStaticPaths } from 'next';
import React, { ReactElement } from 'react';
import { RecurringEvent } from 'types';
import { formatStartEndTime, indexToWeekDay } from 'utilities/date';
import { RecurringEvents } from '../../components/Calendar/RecurringEvents';
import Layout from '../../components/Layout/Layout';
import PageSingle from '../../components/Layout/PageSingle';
import {
  TwoColumns,
  TwoColumnsEnd,
  TwoColumnsStart
} from '../../components/Layout/TwoColumns';
import { NavigationProps } from '../../components/Navigation/Navigation';
import { Seo } from '../../components/Seo/Seo';
import sanityClient from '../../sanityClient';
import { NextPageWithLayout } from '../_app';

type Props = NextPageWithLayout & {
  current: RecurringEvent;
  recurringEvents: RecurringEvent[];
};

const Page = ({ current, recurringEvents }: Props) => {
  return (
    <React.Fragment>
      <Seo
        pageTitle={current.title}
        pageUrl={current.slug.current}
        pageImage={current.image}
      />

      <TwoColumns>
        <TwoColumnsStart>
          <PageSingle
            title={current.title}
            image={current.image}
            metaText={
              <React.Fragment>
                Faste aktivitet | {indexToWeekDay(+current.weekday)} | kl.{' '}
                {formatStartEndTime(current.startTime, current.endTime)} |{' '}
                {current.place}
              </React.Fragment>
            }
            content={current.content}
          />

          {/* {current.image && (
            <img
              src={getImageUrl(current.image)}
              alt={current.title}
              className="w-full object-cover border rounded-lg mb-8 md:mb-12"
            />
          )}

          <div className="bg-white md:pr-20 min-h-[160px] md:min-h-[600px]">
            <PageTitle>{current.title}</PageTitle>

            <p className="mb-6 border-b border-t py-2 border-orange-300">
              Faste aktivitet | {indexToWeekDay(+current.weekday)} | kl.{' '}
              {formatStartEndTime(current.startTime, current.endTime)} |{' '}
              {current.place}
            </p>

            {current.content && (
              <PortableText
                value={current.content}
                components={portableTextSubpage}
              />
            )}
          </div> */}
        </TwoColumnsStart>
        <TwoColumnsEnd>
          <RecurringEvents
            currentEventId={current._id}
            events={recurringEvents}
          />
        </TwoColumnsEnd>
      </TwoColumns>
    </React.Fragment>
  );
};

Page.getLayout = function getLayout(page: ReactElement) {
  return (
    <div className="md:mt-20">
      <Layout navigation={page.props.navigation}>{page}</Layout>
    </div>
  );
};

export default Page;

export async function getStaticProps(props: any) {
  const query = `
    {
      'current': *[_type == "recurring_event" && slug.current == $slug][0]
      {
        _id,
        title,
        calendarTitle,
        weekday,
        startTime,
        endTime,
        place,
        slug,
        image,
        content
      },

      'recurringEvents': *[_type == "recurring_event"]
      {
        _id,
        calendarTitle,
        weekday,
        startTime,
        endTime,
        place,
        slug
      } | order(weekday asc, startTime asc),

      'navigation': *[_type == "siteSettings"][0]
      {
        navigationLinks[]{
          _type,
          _key,
          linkRef->{
            slug
          },
          linkText,
          linkIcon
        },
        facebookUrl,
        instagramUrl
      }
    }
  `;
  const { current, recurringEvents, navigation } = await sanityClient.fetch<{
    current: Event;
    recurringEvents: RecurringEvent[];
    navigation: NavigationProps;
  }>(query, {
    slug: `faste-aktiviteter/${props.params.slug}`
  });

  if (!current) {
    return {
      notFound: true
    };
  }

  return {
    props: {
      current,
      recurringEvents,
      navigation
    }
  };
}

type EventSlug = {
  slug: {
    current: string;
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const query = `
  *[_type == "recurring_event"]
  {
    slug
  }
  `;

  const slugs = await sanityClient.fetch<EventSlug[]>(query);
  const paths = slugs.map((slug: EventSlug) => ({
    params: {
      slug: slug.slug.current.replace('faste-aktiviteter/', '')
    }
  }));

  return { paths, fallback: false };
};
