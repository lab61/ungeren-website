import Link from 'next/link';
import React from 'react';
import { getImageUrl } from '../../helpers/imageHelper';

export type BannerProps = {
  _id: string;
  title: string;
  content: string;
  image: {
    asset: {
      _id: string;
      url: string;
    };
  };

  linkRef: {
    slug: {
      current: string;
    };
  };
  linkText: string;
};

export default function Banner(props: BannerProps) {
  return (
    <div
      className="bg-black rounded-lg overflow-hidden lg:h-[550px] bg-cover bg-center mt-10 lg:mt-16"
      style={{
        backgroundImage: `url(${getImageUrl(props.image)})`
      }}
    >
      <div className="w-full h-full p-6 lg:p-10 flex items-end bg-gradient-to-t from-[rgba(0,0,0,.85)] to-[rgba(0,0,0,.45)]">
        <div className="lg:w-3/5">
          {props.title && (
            <h3 className="text-4xl lg:text-6xl font-bold uppercase text-white whitespace-pre-wrap mb-2">
              {props.title}
            </h3>
          )}

          {props.content && (
            <p className="lg:text-lg text-white mb-12">{props.content}</p>
          )}

          {props.linkRef && props.linkText && (
            <Link
              href={`/${props.linkRef.slug.current}`}
              className="inline-block bg-orange-500 transition duration-75 hover:bg-orange-600 rounded-lg py-3 px-10 lg:text-lg text-white"
            >
              {props.linkText}
            </Link>
          )}
        </div>
      </div>
    </div>
  );
}
