type Props = {
  title: string;
  highlight?: boolean;
  as: 'h1' | 'h2';
};

export const EventTitle = ({ title, highlight = false, as }: Props) => {
  const cssClasss = `
      ${
        highlight ? 'text-white' : 'text-orange-500'
      } text-2xl font-bold whitespace-pre-wrap break-words mr-6`;

  return as === 'h1' ? (
    <h1 className={cssClasss}>{title}</h1>
  ) : (
    <h2 className={cssClasss}>{title}</h2>
  );
};
