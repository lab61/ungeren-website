import { RecurringEvent } from 'types';
import { indexToWeekDay } from 'utilities/date';
import { RecurringEventCard } from './RecurringEventCard';

type Props = {
  currentEventId?: string;
  events: RecurringEvent[];
};

export const RecurringEvents = (props: Props) => {
  // Group events by weekday
  const groupedEvents = props.events.reduce((acc, event) => {
    acc[event.weekday] = acc[event.weekday] || [];
    acc[event.weekday].push(event);
    return acc;
  }, {} as Record<string, RecurringEvent[]>);

  return (
    <div className="bg-black rounded-lg p-8">
      <h3 className="text-2xl font-bold uppercase text-white mb-2">
        Husets faste aktiviteter
      </h3>

      <div className="divide-y-2 divide-gray-600">
        {Object.keys(groupedEvents).map((day) => (
          <div className="py-6" key={day}>
            <h4 className="text-xl font-bold uppercase mb-6 text-orange-500">
              {indexToWeekDay(+day)}
            </h4>

            <div className="flex flex-col gap-y-6">
              {groupedEvents[day].map((event) => (
                <RecurringEventCard
                  active={event._id === props.currentEventId}
                  event={event}
                  key={event._id}
                />
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
