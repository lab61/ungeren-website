import { EventType } from 'types';

type CalendarFilterProps = {
  eventTypes: EventType[];
  filterBy: string;
  handleFilterClick: (eventType: string) => void;
};

export default function CalendarFilter(props: CalendarFilterProps) {
  return (
    <div className="flex flex-wrap sm:justify-start gap-x-2 gap-y-2 sm:gap-x-6 sm:gap-y-0 mb-2 lg:mb-0">
      <Radio
        eventType={{
          _id: 'all',
          name: 'Alle'
        }}
        {...props}
      />
      {props.eventTypes.map((eventType) => (
        <Radio eventType={eventType} {...props} key={eventType._id} />
      ))}
    </div>
  );
}

type RadioProps = {
  eventType: EventType;
  filterBy: string;
  handleFilterClick: (eventType: string) => void;
};

const Radio = (props: RadioProps) => (
  <div className="form-check">
    <input
      className="form-check-input appearance-none rounded-full h-[18px] w-[18px] border-2 border-orange-500 bg-white hover:border-orange-300 checked:bg-orange-500 checked:border-orange-500 focus:outline-none transition duration-200 mt-[3px] align-top bg-no-repeat bg-center bg-contain mr-1.5 cursor-pointer"
      type="radio"
      name="event-type"
      id={`radio-${props.eventType._id}`}
      checked={props.filterBy === props.eventType._id}
      onChange={() => props.handleFilterClick(props.eventType._id)}
    />
    <label
      className="form-check-label inline-block text-gray-800 cursor-pointer"
      htmlFor={`radio-${props.eventType._id}`}
    >
      {props.eventType.name}
    </label>
  </div>
);
