import { motion } from 'framer-motion';
import Link from 'next/link';
import React, { useState } from 'react';
import { Event, EventType, RecurringEvent } from 'types';
import CalendarFilter from './CalendarFilter';
import { EventCard } from './EventCard';
import { RecurringEvents } from './RecurringEvents';

type CalendarProps = {
  events: Event[];
  eventTypes: EventType[];
  recurringEvents: RecurringEvent[];
};

export const Calendar = (props: CalendarProps) => {
  const EVENTS_TO_DISPLAY_INCREMENT = 5;
  const lastYear = new Date().getFullYear() - 1;
  const [eventsToDisplayMultiplier, setEventsToDisplayMultiplier] =
    useState<number>(1);
  const eventsToDisplay =
    eventsToDisplayMultiplier * EVENTS_TO_DISPLAY_INCREMENT;
  const [events, setEvents] = useState<Event[]>(props.events);
  const [filterBy, setFilterBy] = useState<string>('all');
  const nextEvent = filterBy === 'all' ? events[0] : null;
  const visibleEvents =
    filterBy === 'all' ? events.slice(1, eventsToDisplay) : events;
  const noOfHiddenEvents = events.length - eventsToDisplay;

  const handleFilterClick = (eventType: string) => {
    if (eventType === 'all') {
      setFilterBy('all');
      setEvents(props.events);
      return;
    }

    const filteredEvents = props.events.filter(
      (event) => event.event_type?._ref === eventType
    );

    setFilterBy(eventType);
    setEvents(filteredEvents);
    setEventsToDisplayMultiplier(1);
  };

  const handleLoadMoreClick = () => {
    setEventsToDisplayMultiplier(eventsToDisplayMultiplier + 1);
  };

  const item = {
    hidden: {
      opacity: 0,
      y: -20
    },

    show: {
      opacity: 1,
      y: 0
    }
  };

  return (
    <React.Fragment>
      <h1 className="text-4xl font-sans font-extrabold uppercase">Kalender</h1>

      <div className="lg:w-4/6 lg:pr-12">
        <hr className="border-black my-5" />

        <div className="flex flex-col md:justify-between md:flex-row mb-5 md:mb-8">
          <CalendarFilter
            filterBy={filterBy}
            handleFilterClick={handleFilterClick}
            eventTypes={props.eventTypes}
          />

          <div className="flex justify-end">
            <Link
              className="underline hover:no-underline"
              href={`/kalender/arkiv/${lastYear}`}
            >
              Kalender arkivet
            </Link>
          </div>
        </div>
      </div>

      <div className="flex flex-col lg:flex-row gap-6">
        <div className="flex flex-col gap-y-4 lg:gap-y-6 lg:w-4/6">
          {/* Featured event */}
          {nextEvent && <EventCard event={nextEvent} featured />}

          {visibleEvents.map((event, i) => {
            // Calculate transition delay for events
            const previousTotalEvents =
              EVENTS_TO_DISPLAY_INCREMENT * (eventsToDisplayMultiplier - 1);
            const previousMaxIndex = previousTotalEvents - 1;
            const transitionDelay =
              i > previousMaxIndex ? (i - previousMaxIndex) * 0.15 : 0;

            return (
              <motion.div
                variants={item}
                initial="hidden"
                animate="show"
                layout
                transition={{
                  delay: transitionDelay
                }}
                key={event._id}
              >
                <EventCard event={event} />
              </motion.div>
            );
          })}

          {/* Load more events button */}
          {filterBy === 'all' && noOfHiddenEvents > 0 && (
            <button
              className="bg-orange-500 text-white rounded-lg py-4 px-3 mb-4"
              onClick={handleLoadMoreClick}
            >
              Vis flere arrangementer ({noOfHiddenEvents})
            </button>
          )}

          {events.length === 0 && (
            <div className="bg-black rounded-lg py-2 px-3">
              <p className="text-white">
                {filterBy === 'all'
                  ? 'Vi har ingen kommende arrangementer i øjeblikket'
                  : 'Vi har ingen arrangementer der matcher din søgning'}
                <span> ¯\_(ツ)_/¯</span>
              </p>
            </div>
          )}
        </div>

        {/* Recurring events */}
        <RecurringEvents events={props.recurringEvents} />
      </div>
    </React.Fragment>
  );
};
