import Link from 'next/link';
import React from 'react';
import { Event, RecurringEvent } from 'types';
import { FIRST_CALENDAR_YEAR } from '../../constants';
import { EventCard } from './EventCard';
import { RecurringEvents } from './RecurringEvents';

type CalendarProps = {
  currentYear: number;
  events: Event[];
  recurringEvents: RecurringEvent[];
};

export const CalendarArchive = (props: CalendarProps) => {
  const currentYear = new Date().getFullYear();
  const years = Array.from(
    { length: currentYear - FIRST_CALENDAR_YEAR + 1 },
    (_, i) => i + FIRST_CALENDAR_YEAR
  );

  return (
    <React.Fragment>
      <h1 className="text-4xl font-sans font-extrabold uppercase">
        Kalender arkiv {props.currentYear}
      </h1>

      <div className="md:w-4/6 md:pr-12">
        <hr className="border-black my-5" />

        <ul className="flex flex-wrap gap-x-4 mb-8">
          {years.map((year) => (
            <li key={year}>
              <Link
                className={`underline hover:no-underline ${
                  year == props.currentYear ? ' font-bold' : ''
                }`}
                href={`/kalender/arkiv/${year}`}
              >
                {year}
              </Link>
            </li>
          ))}
        </ul>
      </div>

      <div className="flex flex-col lg:flex-row gap-6">
        {/* Events */}
        <div className="flex flex-col gap-y-4 lg:gap-y-6 lg:w-4/6">
          {props.events.map((event) => (
            <EventCard event={event} key={event._id} />
          ))}
        </div>

        {/* Recurring events */}
        <RecurringEvents events={props.recurringEvents} />
      </div>
    </React.Fragment>
  );
};
