import { AnimatePresence, motion } from 'framer-motion';
import Link from 'next/link';
import { useRef, useState } from 'react';
import { FaStar } from 'react-icons/fa';
import { FiChevronDown } from 'react-icons/fi';
import { Event } from 'types';
import { EventArtistsPreview } from './EventArtistsPreview';
import { EventArtistsFull } from './EventArtistsFull';
import { EventContent } from './EventContent';
import { EventDetails } from './EventDetails';
import { EventTitle } from './EventTitle';

const isTouch = () => 'ontouchstart' in window || navigator.maxTouchPoints > 0;

type IEventCard = {
  event: Event;
  featured?: boolean;
};

export const EventCard = ({ event, featured = false }: IEventCard) => {
  const cardRef = useRef<HTMLDivElement>(null);
  const [expanded, setExpanded] = useState(false);
  const handleCardClick = (e: any) => {
    e.preventDefault();
    setExpanded(!expanded);

    // On touch screens clicking links, even with preventDefault,
    // will make the URL bar appear, resulting in a jarring UX.
    if (isTouch()) return;

    if (expanded) {
      window.history.replaceState(null, event.title, '/');
    } else {
      const url = event.slug.current.startsWith('/')
        ? event.slug.current
        : `/${event.slug.current}`;

      window.history.replaceState(null, event.title, url);
    }
  };

  return (
    <Link
      href={event.slug.current}
      prefetch={false}
      scroll={false}
      onClick={handleCardClick}
    >
      <div
        ref={cardRef}
        className={`rounded-lg p-5 relative ${
          featured ? 'bg-orange-500' : 'bg-white border border-black'
        } ${
          expanded
            ? ' cursor-default'
            : ' cursor-pointer hover:opacity-80 hover:border-orange-500 transition-all duration-300'
        }`}
      >
        {/* Featured event text */}
        {featured && (
          <div className="flex gap-2 mb-5">
            <FaStar />
            <h5 className="text-sm font-bold">Næste show</h5>
          </div>
        )}
        <FiChevronDown
          size={24}
          className={`cursor-pointer absolute top-4 right-4 transform transition-transform duration-300 ${
            expanded ? 'rotate-180' : ''
          }`}
        />
        <EventDetails event={event} />
        <EventTitle title={event.title} highlight={featured} as="h2" />
        <EventArtistsPreview artists={event.artists} />

        {/* Content - initially collapsed */}
        <AnimatePresence mode="wait">
          {expanded && (
            <motion.div
              initial={{
                height: 0,
                opacity: 0
              }}
              animate={{
                height: 'auto',
                opacity: 1,
                transition: {
                  height: {
                    duration: 0.4
                  },
                  opacity: {
                    duration: 0.25,
                    delay: 0.15
                  }
                }
              }}
              exit={{
                height: 0,
                opacity: 0,
                transition: {
                  height: {
                    duration: 0.4
                  },
                  opacity: {
                    duration: 0.25
                  }
                }
              }}
              key="collapsed-content"
            >
              <EventContent event={event} />
              <EventArtistsFull artists={event.artists} />
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </Link>
  );
};
