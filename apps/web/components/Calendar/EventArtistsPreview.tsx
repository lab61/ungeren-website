import React from 'react';
import { Artist } from 'types';

type Props = {
  artists: Artist[];
};

export const EventArtistsPreview = ({ artists }: Props) => {
  return (
    <React.Fragment>
      {artists && artists.length > 0 && (
        <ul className="flex flex-row gap-x-2 mt-1">
          {artists.map((artist: Artist, index: number) => (
            <li key={artist._key}>
              <span className="text-lg font-extrabold uppercase">
                {artist.name}
              </span>

              {artist.origin && (
                <span className="font-bold text-sm"> ({artist.origin})</span>
              )}

              {artists.length - 1 > index && (
                <span className="text-lg inline-block -translate-y-[1px] ml-1.5">
                  |
                </span>
              )}
            </li>
          ))}
        </ul>
      )}
    </React.Fragment>
  );
};
