import React from 'react';
import { FiExternalLink } from 'react-icons/fi';
import { Artist } from 'types';

type Props = {
  artists: Artist[];
};

export const EventArtistsFull = ({ artists }: Props) => {
  return (
    <React.Fragment>
      {artists && artists.length > 0 && (
        <ul className="flex flex-col gap-y-5 mt-6">
          {artists.map((artist: Artist) => (
            <li key={artist._key}>
              <div className="mb-2">
                {/* Name */}
                {artist.link ? (
                  <button className="inline-flex items-center gap-x-1.5 underline uppercase">
                    <span>{artist.name}</span>
                    <FiExternalLink />
                  </button>
                ) : (
                  <span className="uppercase">{artist.name}</span>
                )}
              </div>

              {/* Bandcamp embed */}
              {artist.bandcamp_track_id && (
                <div
                  style={{
                    left: 0,
                    width: '100%',
                    height: '42px',
                    position: 'relative'
                  }}
                >
                  <iframe
                    src={`https://bandcamp.com/EmbeddedPlayer/track=${artist.bandcamp_track_id}/size=small/bgcol=333333/linkcol=ffffff/artwork=none/transparent=true/`}
                    sandbox="allow-same-origin allow-scripts"
                    style={{
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      position: 'absolute',
                      border: 0
                    }}
                  ></iframe>
                </div>
              )}
            </li>
          ))}
        </ul>
      )}
    </React.Fragment>
  );
};
