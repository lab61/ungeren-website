import Head from 'next/head';
import { getImageUrl } from '../../helpers/imageHelper';

type SeoProps = {
  pageTitle: string;
  pageUrl: string;
  pageImage?: string;
  seo?: {
    metaTitle?: string;
    metaDescription?: string;
    ogImage?: string;
    ogTitle?: string;
    ogDescription?: string;
  };
};

export const Seo = (props: SeoProps) => {
  const fallbackTitle = props.pageTitle
    .replace(/(\r \ n|\n|\r)/gm, ' ')
    .replace('  ', ' ');
  const metaTitle = props.seo?.metaTitle || fallbackTitle;
  const ogImage = props.seo?.ogImage || props.pageImage;

  return (
    <Head>
      <title>{metaTitle}</title>

      {props.seo?.metaDescription && (
        <meta name="description" content={props.seo?.metaDescription} />
      )}

      <meta property="og:title" content={props.seo?.ogTitle || metaTitle} />

      {(props.seo?.ogDescription || props.seo?.metaDescription) && (
        <meta
          property="og:description"
          content={props.seo?.ogDescription || props.seo?.metaDescription}
        />
      )}

      {ogImage && <meta property="og:image" content={getImageUrl(ogImage)} />}

      <meta
        property="og:url"
        content={`https://www.ungeren.dk${props.pageUrl}`}
      />
      <meta property="og:type" content="website" />
    </Head>
  );
};
