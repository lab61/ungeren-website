export const TwoColumns = ({ children }: { children: React.ReactNode }) => (
  <div className="flex flex-col lg:flex-row gap-6">{children}</div>
);

export const TwoColumnsStart = ({
  children
}: {
  children: React.ReactNode;
}) => <div className="lg:w-4/6">{children}</div>;

export const TwoColumnsEnd = ({ children }: { children: React.ReactNode }) => (
  <div className="lg:w-2/6">{children}</div>
);
