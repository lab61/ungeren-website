import { motion } from 'framer-motion';
import Link from 'next/link';
import { FiHeart } from 'react-icons/fi';

export type HouseRulesProps = {
  rulesTitle: string;
  rulesText: string;
  rulesList: string[];
  rulesLinkRef: {
    slug: {
      current: string;
    };
  };
  rulesLinkText: string;
};

export default function HouseRules(props: HouseRulesProps) {
  return (
    <motion.div className="bg-black" layout>
      <div className="max-w-5xl py-10 lg:py-24 mx-4 px-2 md:px-20 xl:px-8 md:mx-16 lg:mx-auto">
        <h4 className="text-4xl text-orange-500 font-extrabold uppercase mb-7">
          {props.rulesTitle}
        </h4>

        <p className="text-white lg:text-xl mb-8 lg:mb-12 lg:w-10/12">
          {props.rulesText}
        </p>

        <div className="grid grid-cols-1 md:grid-cols-2 gap-y-6 mb-12 lg:mb-20 lg:w-10/12">
          {props.rulesList.map((rule: string) => (
            <div className="flex flex-row items-center gap-x-3" key={rule}>
              <FiHeart className="inline-block w-6 h-6 text-orange-500" />
              <p className="text-white lg:text-xl">{rule}</p>
            </div>
          ))}
        </div>

        {props.rulesLinkRef.slug && (
          <Link
            href={props.rulesLinkRef.slug.current}
            className="inline-block bg-white rounded-xl px-8 py-3 lg:text-xl transition-colors duration-200 hover:bg-slate-300"
          >
            {props.rulesLinkText}
          </Link>
        )}
      </div>
    </motion.div>
  );
}
