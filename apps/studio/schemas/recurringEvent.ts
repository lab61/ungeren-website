import {CiRepeat} from 'react-icons/ci'
import {defineField, defineType} from 'sanity'
import {indexToWeekDay} from 'utilities/date'
import {stringToSlug} from '../helpers/slug-helper'

export default defineType({
  name: 'recurring_event',
  type: 'document',
  icon: CiRepeat,
  title: 'Recurring event',
  fields: [
    defineField({
      title: 'Calendar title',
      name: 'calendarTitle',
      type: 'string',
      description: 'Displayed in the calender',
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'Weekday',
      name: 'weekday',
      type: 'number',
      options: {
        list: [
          {title: 'Mandag', value: 0},
          {title: 'Tirsdag', value: 1},
          {title: 'Onsdag', value: 2},
          {title: 'Torsdag', value: 3},
          {title: 'Fredag', value: 4},
          {title: 'Lørdag', value: 5},
          {title: 'Søndag', value: 6},
        ],
        layout: 'dropdown',
      },
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'Start time',
      name: 'startTime',
      type: 'number',
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'End time',
      name: 'endTime',
      type: 'number',
    }),
    defineField({
      title: 'Place',
      name: 'place',
      type: 'string',
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'Title',
      name: 'title',
      type: 'string',
      description: 'Displayed on the event page',
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'Slug',
      name: 'slug',
      type: 'slug',
      options: {
        source: (doc: any) => {
          return stringToSlug(doc.title).slice(0, 200)
        },
        slugify: (input) => 'faste-aktiviteter/' + input,
      },
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'Image',
      name: 'image',
      type: 'image',
    }),
    defineField({
      title: 'Content',
      name: 'content',
      description: 'Displayed on the event page',
      type: 'pageContent',
    }),
  ],
  preview: {
    select: {
      title: 'title',
      weekday: 'weekday',
      startTime: 'startTime',
      endTime: 'endTime',
    },
    prepare(selection: any) {
      const {title, weekday, startTime, endTime} = selection
      return {
        title: title,
        subtitle: `${indexToWeekDay(weekday)} kl. ${startTime}${endTime ? ' - ' + endTime : ''}`,
      }
    },
  },
  // orderings: [
  //   {
  //     title: 'Weekday',
  //     name: 'weekday',

  //     by: [
  //       {
  //         field: 'weekday',
  //         direction: 'asc',
  //       },
  //       {
  //         field: 'startTime',
  //         direction: 'asc',
  //       },
  //     ],
  //   },
  // ],
})
