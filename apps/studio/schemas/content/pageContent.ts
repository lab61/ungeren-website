import {defineArrayMember, defineField} from 'sanity'

export default defineField({
  title: 'Page content',
  name: 'pageContent',
  type: 'array',
  of: [
    defineArrayMember({
      type: 'block',
      styles: [
        {title: 'Normal', value: 'normal'},
        {title: '2. header (xxlarge, orange)', value: 'h2'},
        {title: '3. header (xlarge, orange)', value: 'h3'},
        {title: '4. header (large, black)', value: 'h4'},
      ],
      marks: {
        decorators: [
          {title: 'Strong', value: 'strong'},
          {title: 'Emphasis', value: 'em'},
        ],
        annotations: [
          {
            title: 'URL',
            name: 'link',
            type: 'object',
            fields: [
              {
                title: 'URL',
                name: 'href',
                type: 'url',
              },
            ],
          },
        ],
      },
      lists: [
        {title: 'Bullet', value: 'bullet'},
        {title: 'Number', value: 'number'},
      ],
    }),
    defineArrayMember({
      type: 'gallery',
    }),
  ],
  group: 'page',
})
