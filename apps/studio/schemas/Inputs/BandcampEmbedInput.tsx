import {Stack, TextInput} from '@sanity/ui'
import {set, unset} from 'sanity'

export const BandcampEmbedInput = (props: any) => {
  const {elementProps, onChange, value = ''} = props

  const handleChange = (event: any) => {
    let nextValue = event.currentTarget.value

    // Extract "track=xxx" from event.target.value
    // and set it as the value of the field
    const match = nextValue.match(/track=(\d+)/)
    if (match) {
      nextValue = match[1]
    }

    onChange(nextValue ? set(nextValue) : unset())
  }

  return (
    <Stack space={2}>
      <TextInput {...elementProps} onChange={handleChange} value={value} />
    </Stack>
  )
}
