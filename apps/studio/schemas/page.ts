import {CiStickyNote} from 'react-icons/ci'
import {defineArrayMember, defineField, defineType} from 'sanity'

export default defineType({
  title: 'Page',
  name: 'page',
  type: 'document',
  icon: CiStickyNote,
  groups: [
    {
      name: 'page',
      title: 'Page',
      default: true,
    },
    {
      name: 'seo',
      title: 'SEO',
    },
  ],
  fields: [
    defineField({
      title: 'Title',
      name: 'title',
      type: 'text',
      rows: 3,
      group: 'page',
    }),
    defineField({
      title: 'Image',
      name: 'image',
      type: 'image',
      group: 'page',
    }),
    defineField({
      title: 'Slug',
      name: 'slug',
      type: 'slug',
      options: {
        source: 'title',
      },
      group: 'page',
    }),
    defineField({
      title: 'Content',
      name: 'content',
      type: 'pageContent',
      group: 'page',
    }),
    defineField({
      name: 'seo',
      type: 'seo',
      group: 'seo',
    }),
  ],
  preview: {
    select: {
      title: 'title',
    },
  },
})
