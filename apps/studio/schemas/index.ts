import artist from './artist'
import event from './event'
import eventType from './eventType'
import recurringEvent from './recurringEvent'
import siteSettings from './siteSettings'
import news from './news'
import page from './page'
import banner from './banner'
import seo from './blocks/seo'
import gallery from './blocks/gallery'
import pageContent from './content/pageContent'

export const schemaTypes = [
  seo,
  gallery,
  pageContent,

  event,
  eventType,
  artist,
  banner,
  recurringEvent,
  news,
  page,
  siteSettings,
]
