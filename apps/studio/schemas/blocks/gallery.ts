import {defineField, defineType} from 'sanity'

export default defineType({
  title: 'Gallery',
  name: 'gallery',
  type: 'object',
  fields: [
    defineField({
      title: 'Images',
      name: 'images',
      type: 'array',
      of: [
        {
          title: 'Image',
          name: 'image',
          type: 'image',
          options: {
            hotspot: false,
            crop: false,
          },
        },
      ],
      options: {
        layout: 'grid',
      },
    }),
  ],
  preview: {
    select: {
      images: 'images',
    },
    prepare(selection) {
      const {images} = selection
      const image = images.find((image: any) => image)

      return {
        title: `Gallery block of ${Object.keys(images).length} images`,
        media: image,
      }
    },
  },
})
