import {CiImageOn} from 'react-icons/ci'
import {defineField, defineType} from 'sanity'

export default defineType({
  title: 'Banner',
  name: 'banner',
  type: 'document',
  icon: CiImageOn,
  fields: [
    defineField({
      title: 'Title',
      name: 'title',
      type: 'text',
      rows: 3,
    }),
    defineField({
      title: 'Image',
      name: 'image',
      type: 'image',
      validation: (Rule) => Rule.required(),
    }),
    defineField({
      title: 'Content',
      name: 'content',
      type: 'text',
      rows: 5,
    }),
    defineField({
      title: 'Link',
      name: 'linkRef',
      type: 'reference',
      to: [{type: 'page'}],
    }),
    defineField({
      title: 'Link Text',
      name: 'linkText',
      type: 'string',
    }),
  ],
  preview: {
    select: {
      title: 'title',
    },
  },
})
