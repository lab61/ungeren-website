import {defineConfig} from 'sanity'
import {deskTool} from 'sanity/desk'
import {visionTool} from '@sanity/vision'
import {media} from 'sanity-plugin-media'
import {schemaTypes} from './schemas'
import {theme} from './theme'
import {deskStructure} from './deskStructure'

export default defineConfig({
  name: 'default',
  title: 'Ungeren Studio',
  projectId: 'lfobj7pl',
  dataset: 'production',
  theme,
  plugins: [deskTool({structure: deskStructure}), visionTool(), media()],
  schema: {
    types: schemaTypes,
  },
})
