export function stringToSlug(str: string) {
  str = str.replace(/^\s+|\s+$/g, '') // trim
  str = str.toLowerCase()

  // remove parantheses characters
  str = str.replace(/[\(\)]/g, '')

  // replace linebreaks with dashes
  str = str.replace(/\n/g, '-')

  // swap æ, ø å
  str = str.replace(/æ/g, 'ae')
  str = str.replace(/ø/g, 'oe')
  str = str.replace(/å/g, 'aa')

  // remove accents, swap ñ for n, etc
  var from = 'àáäâèéëêìíïîòóöôùúüûñç·_/,:;'
  var to = 'aaaaeeeeiiiioooouuuunc------'
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
  }

  str = str
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/[^a-z0-9\\-]/g, '') // remove anything not alphanumeric or a dash
    .replace(/-+/g, '-') // Remove multiple dashes

  return str
}
