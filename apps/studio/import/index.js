const jsdom = require('jsdom')
const ndjson = require('ndjson')
const fs = require('fs')
const blockTools = require('@sanity/block-tools')
const Schema = require('@sanity/schema')
const shows = require('./raw.js')

const defaultSchema = Schema.compile({
  name: 'shows',
  types: [
    {
      name: 'show',
      type: 'document',
      title: 'Show',
      fields: [
        {
          name: 'title',
          type: 'text',
          title: 'Title',
          rows: 3,
        },
        {
          name: 'date',
          type: 'datetime',
          title: 'Date',
        },
        {
          name: 'price',
          type: 'string',
          title: 'Price',
        },
        {
          name: 'image',
          type: 'image',
          title: 'Image',
        },
        {
          name: 'slug',
          type: 'slug',
          title: 'Slug',
          options: {
            source: 'title',
          },
        },
        {
          name: 'legacy_slug',
          type: 'string',
          title: 'Legacy Slug',
        },
        {
          title: 'Content',
          name: 'content',
          type: 'array',
          of: [{type: 'block'}],
        },
      ],
    },
  ],
})

const {JSDOM} = jsdom
let ndJsonLines = ''
const ndSerialize = ndjson.stringify()
ndSerialize.on('data', (line) => (ndJsonLines += `${line}`))

const writeJson = (obj) => ndSerialize.write(obj)
const blockContentType = defaultSchema
  .get('show')
  .fields.find((field) => field.name === 'content').type

async function main() {
  const posts = shows.slice(0, 2000)
  for (const post of posts) {
    await processPost(post)
  }

  fs.writeFileSync('import/output.ndjson', ndJsonLines, (err) => {
    if (err) throw err
    console.log('The file has been saved!')
  })
}

main()

async function processPost(post) {
  const {id, title, date, price, url, text} = post

  let html = text
  html = html.replace(/&amp;/g, '&')
  html = html.replace(/&lt;/g, '<')
  html = html.replace(/&gt;/g, '>')
  html = html.replace(/&quot;/g, '"')
  html = html.replace(/&#039;/g, "'")

  // find first image in html and use that as main image
  let image = undefined
  const firstImage = html.match(/<img[^>]+>/)
  if (firstImage && firstImage.length > 0 && firstImage[0].includes('src')) {
    const imageValue = firstImage[0].match(/src="([^"]+)"/)
    if (imageValue && imageValue.length > 1) {
      let src = imageValue[1]

      // Skip images that contains "fbcdn"
      const ignore = [
        'fbcdn',
        'akpress',
        'pinnerup.anarcho',
        'gif_balkan_boogie_circusnight',
        'metal-for-asyl',
        'img375.imageshack.us',
      ]
      if (!ignore.some((ignore) => src.includes(ignore))) {
        // if source is relative, add domain
        if (!src.startsWith('http')) {
          src = `http://ungeren.dk${src}`
        }

        src = src.replace(/&oslash;/g, 'ø')
        src = src.replace(/&aring;/g, 'å')
        src = src.replace(/&Aring;/g, 'Å')
        src = src.replace(/&aelig;/g, 'æ')
        src = src.replace(/&uuml;/g, 'ü')

        image = {_type: 'image', _sanityAsset: `image@${src}`}
      }
    }
  }

  // Remove all images from html
  html = html.replace(/<img[^>]+>/g, '')
  html = html.replace(/<p><\/p>/g, '')
  html = html.replace(/<p>\s*<\/p>/g, '')
  html = html.replace(/<p>\s*<br>\s*<\/p>/g, '')

  let content = await convertHtmlToBlock(html)

  // Remove empty first and last block
  content = content.filter((block, index) => {
    if (
      (index === 0 || index === content.length - 1) &&
      block.children.length === 1 &&
      block.children[0]._type === 'span' &&
      block.children[0].text === ''
    ) {
      return false
    }

    if (
      block.children.length === 1 &&
      block.children[0]._type === 'span' &&
      block.children[0].text === '\n'
    ) {
      return false
    }

    return true
  })

  writeJson({
    _id: `show-${id}`,
    _type: 'show',
    title,
    date,
    image,
    slug: {_type: 'slug', current: url},
    price,
    content,
  })
}

async function convertHtmlToBlock(html) {
  return blockTools.htmlToBlocks(html, blockContentType, {
    parseHtml: (html) => {
      return new JSDOM(`<!DOCTYPE html><html><body>${html}</body></html>`).window.document
    },
    // rules: [
    //   {
    //     deserialize(el, next, block) {
    //       if (el?.tagName?.toLowerCase() === 'img') {
    //         return block({
    //           _type: 'image',
    //           _sanityAsset: `image@http://ungeren.dk${el.getAttribute('src')}`,
    //         })
    //       }
    //       return undefined
    //     },
    //   },
    // ],
  })
}
